﻿using FileSystem;
class Program
{
    static void Main(string[] args)
    {
        FileSystem.FileSystem fileSystem = new FileSystem.FileSystem();

        string input = Console.ReadLine();   

        while (!input.StartsWith("exit"))
        {
            
            string[] inputArray = input.Split(' ');            
            
            string command = inputArray[0];          

            switch (command)
            {
            
                case "ls":
                    fileSystem.ls();
                    break;

                case "cd":
                    string subCommandContent = inputArray[1];
                    fileSystem.cd(subCommandContent);
                    break;

                case "mkdir":
                    subCommandContent = inputArray[1];
                    fileSystem.mkdir(subCommandContent);
                    break;

                case "touch":
                    string textFile = "";
                    for (int i = 2; i < inputArray.Length; i++)
                    {
                        textFile += inputArray[i];
                        textFile += " ";
                    }

                    subCommandContent = inputArray[1];                    
                    fileSystem.touch(subCommandContent, textFile);
                    break;

                case "wc":
                    
                        string text = "";
                        for (int i = 1; i < inputArray.Length; i++)
                        {
                            text += inputArray[i];
                            text += " ";
                        }
                        fileSystem.wc(text);
                                        
                    break;
            }

            input = Console.ReadLine();
        }        
       
    }
}