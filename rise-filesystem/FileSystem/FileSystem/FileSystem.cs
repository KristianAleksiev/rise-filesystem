﻿using System.IO;
using System.Text.RegularExpressions;

namespace FileSystem
{
    public class FileSystem
    {
        string currentDirectory;
        Dictionary<string, string> fileSystem;

        public FileSystem()
        {
            currentDirectory = "/";
            fileSystem = new Dictionary<string, string>();

        }

        public void ls()
        {
            Console.WriteLine($"Contents of directory {currentDirectory}:");

            foreach (KeyValuePair<string, string> entry in fileSystem)
            {

                if (entry.Key.StartsWith(currentDirectory) && !entry.Key.Equals("/"))
                {

                    Console.WriteLine(entry.Key.Replace('\\', '/'));
                }

            }
        }

        public void mkdir(string directory)
        {
            string newDirectory = Path.Combine(currentDirectory, directory);

            if (fileSystem.ContainsKey(newDirectory))
            {
                Console.WriteLine("Directory already exists.");
            }
            else
            {
                fileSystem.Add(newDirectory, "");
                Console.WriteLine("Directory created.");
            }
        }

        public void cat(string file)
        {
            string path = Path.Combine(currentDirectory, file);

            if (fileSystem.ContainsKey(path))
            {
                Console.WriteLine(fileSystem[path]);
            }
            else
            {
                Console.WriteLine("File not found.");
            }
        }

        public void cd(string directory)
        {
            string newDirectory = Path.Combine(currentDirectory, directory);

            if (fileSystem.ContainsKey(newDirectory))
            {
                currentDirectory = newDirectory;
            }
            else
            {
                Console.WriteLine("Directory not found.");
            }
        }

        public void touch(string file, string contents)

        {
            if (file.Contains("/"))
            {
                Console.WriteLine("Inv`alid file name");
            }
            else
            {
                string path = Path.Combine(currentDirectory, file);
                if (fileSystem.ContainsKey(path))
                {
                    Console.WriteLine("File already exists.");
                }
                else
                {
                    fileSystem.Add(path, contents);
                    Console.WriteLine("File created.");
                    Console.WriteLine(fileSystem[path]);
                }
            }
        }
      

        public void wc<inputType>(inputType fileOrText)
        {
            string path = Path.Combine(currentDirectory, fileOrText.ToString());

            /*foreach (KeyValuePair<string, string> entry in fileSystem)
            {
                Console.WriteLine(entry.Key, entry.Value);
            }*/


            if (fileSystem.ContainsKey(path)) // <==================
            {
                string contents = fileSystem[path];
                Console.WriteLine(contents);
                Console.WriteLine("=====");

                if (string.IsNullOrEmpty(contents))
                {
                    Console.WriteLine("File content is empty.");
                }

                string[] fileWords = contents.Split( ' ');

                //string[] words = Regex.Split(contents, @"\W+");
                //int fileWordsCount = fileWords.Length;

                Console.WriteLine($"Number of words in : {fileWords.Length}");
                //Console.WriteLine($"Number of words in {fileOrText}: {fileWords.Length}");
            }
            else
            {
                string[] fileWords = Regex.Split(fileOrText.ToString(), @"\W+");
                Console.WriteLine($"Number of words: {fileWords.Length - 1}");                
            }
        }
    }
}
